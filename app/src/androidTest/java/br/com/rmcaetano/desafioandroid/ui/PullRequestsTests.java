package br.com.rmcaetano.desafioandroid.ui;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.parceler.Parcels;

import br.com.rmcaetano.desafioandroid.R;
import br.com.rmcaetano.desafioandroid.network.Api;
import br.com.rmcaetano.desafioandroid.network.Owner;
import br.com.rmcaetano.desafioandroid.network.Repository;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class PullRequestsTests {

    public static final String TEST_TEXT = "TEST";
    public static final String TEST_TITLE = "TEST-FULL-NAME";

    @Rule
    public ActivityTestRule<PullRequestsActivity> testRule = new ActivityTestRule<>(
            PullRequestsActivity.class,
            true,
            false);

    @Before
    public void before() {
        Api.initMock(InstrumentationRegistry.getContext());
    }

    @Test
    public void successfullyRetry() {
        Api.setMockError(true);

        startActivityWithRepository();

        onView(withText(R.string.retry_alert_message))
                .check(matches(isCompletelyDisplayed()));

        Api.setMockError(false);

        onView(withText(R.string.retry_alert_retry))
                .perform(click());

        onView(withText("Pull request description 1"))
                .check(matches(isCompletelyDisplayed()));
    }

    @Test
    public void assertRepositoryName() {
        Api.setMockError(false);

        startActivityWithRepository();

        onView(withText(TEST_TITLE))
                .check(matches(isCompletelyDisplayed()));
    }

    private void startActivityWithRepository() {
        Repository repository = new Repository();
        repository.name = TEST_TEXT;
        repository.fullName = TEST_TITLE;
        repository.owner = new Owner();
        repository.owner.login = TEST_TEXT;

        Intent intent = new Intent();
        intent.putExtra(PullRequestsActivity.REPOSITORY, Parcels.wrap(repository));

        testRule.launchActivity(intent);
    }
}