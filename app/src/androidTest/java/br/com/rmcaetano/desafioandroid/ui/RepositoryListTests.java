package br.com.rmcaetano.desafioandroid.ui;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.rmcaetano.desafioandroid.R;
import br.com.rmcaetano.desafioandroid.network.Api;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class RepositoryListTests {

    @Rule
    public ActivityTestRule<RepositoryListActivity> testRule = new ActivityTestRule<>(
            RepositoryListActivity.class,
            true,
            false);

    @Before
    public void before() {
        Api.initMock(InstrumentationRegistry.getContext());
    }

    @Test
    public void successfullyRetry() {
        Api.setMockError(true);

        testRule.launchActivity(new Intent());

        onView(withText(R.string.retry_alert_message))
                .check(matches(isCompletelyDisplayed()));

        Api.setMockError(false);

        onView(withText(R.string.retry_alert_retry))
                .perform(click());

        onView(withText("Repository"))
                .check(matches(isCompletelyDisplayed()));
    }

    @Test
    public void showRepositoryPullRequests() {
        testRule.launchActivity(new Intent());

        onView(withText("Repository"))
                .perform(click());

        onView(withText("Pull request description 1"))
                .check(matches(isCompletelyDisplayed()));
    }
}