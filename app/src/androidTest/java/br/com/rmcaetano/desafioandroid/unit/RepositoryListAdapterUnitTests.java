package br.com.rmcaetano.desafioandroid.unit;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import br.com.rmcaetano.desafioandroid.code.RepositoryListAdapter;
import br.com.rmcaetano.desafioandroid.network.Api;
import br.com.rmcaetano.desafioandroid.network.Repository;
import br.com.rmcaetano.desafioandroid.ui.RepositoryListActivity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(AndroidJUnit4.class)
public class RepositoryListAdapterUnitTests {

    private RepositoryListAdapter adapter;

    @Rule
    public ActivityTestRule<RepositoryListActivity> testRule = new ActivityTestRule<>(RepositoryListActivity.class);

    @Before
    public void initAdapter() {
        Api.initMock(testRule.getActivity());
        adapter = new RepositoryListAdapter();
    }

    @Test
    public void estimateProbablyHasMoreItem() {
        assertThat(adapter.getItemCount(), is(0));

        adapter.setData(new ArrayList<Repository>());
        assertThat(adapter.getItemCount(), is(0));

        final ArrayList<Repository> data = new ArrayList<>();
        data.add(new Repository());
        adapter.addToData(data);
        assertThat(adapter.getItemCount(), is(1));

        data.clear();
        while (data.size() < 20) {
            data.add(new Repository());
        }

        adapter.addToData(data);
        assertThat(adapter.getItemCount(), is(21));
    }
}