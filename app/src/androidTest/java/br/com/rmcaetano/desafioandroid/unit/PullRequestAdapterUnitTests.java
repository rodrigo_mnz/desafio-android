package br.com.rmcaetano.desafioandroid.unit;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import br.com.rmcaetano.desafioandroid.code.PullRequestsAdapter;
import br.com.rmcaetano.desafioandroid.network.Api;
import br.com.rmcaetano.desafioandroid.network.PullRequest;
import br.com.rmcaetano.desafioandroid.ui.RepositoryListActivity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

@RunWith(AndroidJUnit4.class)
public class PullRequestAdapterUnitTests {

    private PullRequestsAdapter adapter;

    @Rule
    public ActivityTestRule<RepositoryListActivity> testRule = new ActivityTestRule<>(RepositoryListActivity.class);

    @Before
    public void initAdapter() {
        Api.initMock(testRule.getActivity());
        adapter = new PullRequestsAdapter(testRule.getActivity());
    }

    @Test
    public void estimateProbablyHasMoreItem() {
        assertThat(adapter.getItemCount(), is(0));

        adapter.setData(new ArrayList<PullRequest>());
        assertThat(adapter.getItemCount(), is(0));

        final ArrayList<PullRequest> data = new ArrayList<>();
        data.add(new PullRequest());
        adapter.addToData(data);
        assertThat(adapter.getItemCount(), is(1));

        data.clear();
        while (data.size() < 20) {
            data.add(new PullRequest());
        }

        adapter.addToData(data);
        assertThat(adapter.getItemCount(), is(21));
    }

    @Test
    public void generateCorrectDate() {
        final String generatedDate = adapter.generateDate("2013-01-08T20:11:48Z");
        assertThat(generatedDate, is("08/01/13 20:11"));
    }

    @Test
    public void generateWrongDate() {
        final String generatedDate = adapter.generateDate("00000000000000000");
        assertThat(generatedDate, isEmptyOrNullString());
    }

    @Test
    public void setNullDataSafely() {
        assertThat(adapter.getData(), is(nullValue()));

        adapter.setData(new ArrayList<PullRequest>());
        assertThat(adapter.getData(), not(nullValue()));

        adapter.setData(null);
        assertThat(adapter.getData(), not(nullValue()));
    }

    @Test
    public void returnCorrectItemCount() {
        assertThat(adapter.getItemCount(), is(0));

        adapter.setData(new ArrayList<PullRequest>());
        adapter.getData().add(new PullRequest());

        assertThat(adapter.getItemCount(), is(1));
    }

    @Test
    public void sortPullRequests() {
        PullRequest open = new PullRequest();
        open.state = "OPEN";

        PullRequest closed = new PullRequest();
        closed.state = "CLOSED";

        ArrayList<PullRequest> list = new ArrayList<>();
        list.add(closed);
        list.add(open);

        assertThat(list.get(0).state, equalTo("CLOSED"));

        adapter.sortOpenPullRequestFirst(list);

        assertThat(list.get(0).state, equalTo("OPEN"));
    }
}