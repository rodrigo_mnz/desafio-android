package br.com.rmcaetano.desafioandroid.code;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import br.com.rmcaetano.desafioandroid.R;
import br.com.rmcaetano.desafioandroid.ui.BaseActivity;

public class BaseController {

    protected void showProgressBar(BaseActivity activity, boolean show) {

        if (activity != null && !activity.isFinishing()) {
            activity.showProgressBar(show);
        }
    }

    protected void showRetryDialog(Activity context, final LoadDataCallbacks callbacks) {
        if (context != null && !context.isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.retry_alert_title)
                    .setMessage(R.string.retry_alert_message)
                    .setPositiveButton(R.string.retry_alert_retry, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (callbacks != null) {
                                callbacks.onRetry();
                            }
                        }
                    })
                    .setNegativeButton(R.string.retry_alert_cancel, null);

            builder.create().show();
        }
    }
}