package br.com.rmcaetano.desafioandroid.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.parceler.Parcels;

import java.util.ArrayList;

import br.com.rmcaetano.desafioandroid.R;
import br.com.rmcaetano.desafioandroid.code.RepositoryListAdapter;
import br.com.rmcaetano.desafioandroid.code.RepositoryListController;
import br.com.rmcaetano.desafioandroid.network.Repository;

public class RepositoryListActivity extends BaseActivity {

    public static final String DATA = "DATA";
    private RepositoryListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RepositoryListAdapter();
        recyclerView.setAdapter(adapter);

        RepositoryListController controller = new RepositoryListController(this, adapter);

        if (savedInstanceState != null) {
            final ArrayList<Repository> data = Parcels.unwrap(savedInstanceState.getParcelable(DATA));
            adapter.setData(data);
        } else {
            controller.fetchRepositories(1);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(DATA, Parcels.wrap(adapter.getData()));
        super.onSaveInstanceState(outState);
    }
}