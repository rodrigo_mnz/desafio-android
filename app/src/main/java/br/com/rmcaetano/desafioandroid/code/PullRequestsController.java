package br.com.rmcaetano.desafioandroid.code;

import android.content.Intent;
import android.net.Uri;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import br.com.rmcaetano.desafioandroid.network.Api;
import br.com.rmcaetano.desafioandroid.network.PullRequest;
import br.com.rmcaetano.desafioandroid.ui.PullRequestsActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullRequestsController extends BaseController implements ListItemClickListener<PullRequest>, LoadDataCallbacks {

    private WeakReference<PullRequestsActivity> activity = new WeakReference<>(null);
    private final PullRequestsAdapter adapter;
    private int lastPageLoaded;
    private String ownerLogin;
    private String repositoryName;

    public PullRequestsController(PullRequestsActivity activity, PullRequestsAdapter adapter) {
        this.activity = new WeakReference<>(activity);
        this.adapter = adapter;

        this.adapter.setOnItemClickListener(this);
        this.adapter.setLoadDataCallbacks(this);
    }

    @Override
    public void onLoadMore() {
        lastPageLoaded++;
        fetchPullRequests(ownerLogin, repositoryName, lastPageLoaded);
    }

    @Override
    public void onRetry() {
        fetchPullRequests(ownerLogin, repositoryName, lastPageLoaded);
    }

    @SuppressWarnings("unused")
    @Override
    public void onItemClick(PullRequest item) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.htmlUrl));
        activity.get().startActivity(browserIntent);
    }

    public void fetchPullRequests(String ownerLogin, String repositoryName, final int page) {
        this.ownerLogin = ownerLogin;
        this.repositoryName = repositoryName;
        lastPageLoaded = page;

        if (page == 1) {
            showProgressBar(activity.get(), true);
        }

        Api.getService().getPullRequests(ownerLogin, repositoryName, page).enqueue(new Callback<ArrayList<PullRequest>>() {
            @Override
            public void onResponse(Call<ArrayList<PullRequest>> call, Response<ArrayList<PullRequest>> response) {
                final ArrayList<PullRequest> pullRequestLists = response.body();

                if (activity.get() != null && pullRequestLists != null && !activity.get().isFinishing()) {
                    updateAdapter(pullRequestLists, page);
                }
                if (page == 1) {
                    showProgressBar(activity.get(), false);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PullRequest>> call, Throwable t) {
                t.printStackTrace();

                showRetryDialog(activity.get(), PullRequestsController.this);

                if (page == 1) {
                    showProgressBar(activity.get(), false);
                }
            }
        });
    }

    private void updateAdapter(ArrayList<PullRequest> pullRequests, int page) {
        if (page == 1) {
            adapter.setData(pullRequests);
        } else {
            adapter.addToData(pullRequests);
        }
    }
}