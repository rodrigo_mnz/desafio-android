package br.com.rmcaetano.desafioandroid.network;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/search/repositories?q=language:Java&sort=stars")
    Call<RepositoryList> getRepositories(@Query("page") int page);

    @GET("/repos/{owner}/{repository}/pulls?state=all")
    Call<ArrayList<PullRequest>> getPullRequests(@Path("owner") String owner,
                                                 @Path("repository") String repository,
                                                 @Query("page") int page);
}