package br.com.rmcaetano.desafioandroid.code;

interface LoadDataCallbacks {

    void onLoadMore();
    
    void onRetry();
}