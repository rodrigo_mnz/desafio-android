package br.com.rmcaetano.desafioandroid;

import android.app.Application;

import br.com.rmcaetano.desafioandroid.network.Api;

public class CustomApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Api.initApi(this);
    }
}