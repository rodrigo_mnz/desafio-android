package br.com.rmcaetano.desafioandroid.network;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MockInterceptor implements Interceptor {

    private final String repositoryListResponse;
    private final String pullRequestsResponse;
    private boolean error;

    public MockInterceptor(Context context) {
        repositoryListResponse = getJson(context, "repository_list.json");
        pullRequestsResponse = getJson(context, "pull_request.json");
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        String responseString = null;

        if (chain.request().url().pathSegments().get(0).equalsIgnoreCase("search")) {
            responseString = repositoryListResponse;
        } else if (chain.request().url().pathSegments().get(3).equalsIgnoreCase("pulls")) {
            responseString = pullRequestsResponse;
        }

        if (responseString != null) {
            if (error) {
                throw new IOException();
            } else {
                return new Response.Builder()
                        .code(200)
                        .message(responseString)
                        .request(chain.request())
                        .protocol(Protocol.HTTP_1_0)
                        .body(ResponseBody.create(MediaType.parse("application/json"), responseString.getBytes()))
                        .addHeader("content-type", "application/json")
                        .build();
            }
        }

        return chain.proceed(chain.request());
    }

    public void forceError(boolean error) {
        this.error = error;
    }

    private static String getJson(Context context, String fileName) {
        AssetManager assetManager = context.getAssets();
        ByteArrayOutputStream outputStream = null;
        InputStream inputStream;
        try {
            inputStream = assetManager.open(fileName);
            outputStream = new ByteArrayOutputStream();
            byte buf[] = new byte[1024];
            int len;
            try {
                while ((len = inputStream.read(buf)) != -1) {
                    outputStream.write(buf, 0, len);
                }
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (outputStream != null) {
            return outputStream.toString();
        } else {
            return null;
        }
    }
}