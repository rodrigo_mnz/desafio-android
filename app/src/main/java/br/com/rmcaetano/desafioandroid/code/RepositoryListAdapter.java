package br.com.rmcaetano.desafioandroid.code;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.rmcaetano.desafioandroid.R;
import br.com.rmcaetano.desafioandroid.network.Repository;

public class RepositoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int NORMAL_VIEW = 0;
    private static final int FOOTER_VIEW = 1;
    public static final int ITEMS_PER_PAGE = 30;
    private boolean probablyHasMoreItems;
    private ArrayList<Repository> data;
    private LoadDataCallbacks loadDataCallbacks;
    private ListItemClickListener<Repository> listener;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == NORMAL_VIEW) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_repository, parent, false);
            return new ItemViewHolder(v);
        } else if (viewType == FOOTER_VIEW) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_progressbar, parent, false);
            return new FooterViewHolder(v);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (getItemViewType(position) == NORMAL_VIEW) {

            final Repository repo = data.get(position);
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.name.setText(repo.fullName);
            itemViewHolder.description.setText(repo.description);
            itemViewHolder.owner.setText(repo.owner.login);

            itemViewHolder.forks.setText(String.valueOf(repo.forksCount));
            itemViewHolder.stars.setText(String.valueOf(repo.stargazersCount));

            Picasso.with(holder.itemView.getContext())
                    .load(repo.owner.avatarUrl)
                    .placeholder(R.drawable.ic_person)
                    .into(itemViewHolder.avatar);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(repo);
                    }
                }
            });
        } else if (getItemViewType(position) == FOOTER_VIEW) {

            if (loadDataCallbacks != null) {
                loadDataCallbacks.onLoadMore();
            }
        }
    }

    @Override
    public int getItemViewType(final int position) {

        if ((position == data.size()))
            return FOOTER_VIEW;

        return NORMAL_VIEW;
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }

        return probablyHasMoreItems ? data.size() + 1 : data.size();
    }

    public void setData(ArrayList<Repository> data) {
        if (data != null) {
            probablyHasMoreItems = data.size() >= ITEMS_PER_PAGE;

            this.data = data;
            notifyDataSetChanged();
        }
    }

    public void addToData(List<Repository> data) {
        if (data != null) {
            probablyHasMoreItems = data.size() >= ITEMS_PER_PAGE;

            final int initialSize = this.data.size();
            this.data.addAll(data);
            notifyItemRangeChanged(initialSize, this.data.size());
        }
    }

    public void setLoadDataCallbacks(LoadDataCallbacks loadDataCallbacks) {
        this.loadDataCallbacks = loadDataCallbacks;
    }

    public void setOnItemClickListener(ListItemClickListener<Repository> listener) {
        this.listener = listener;
    }

    public ArrayList<Repository> getData() {
        return data;
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        final TextView name;
        final TextView description;
        final TextView forks;
        final TextView stars;
        final TextView owner;
        final ImageView avatar;

        ItemViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.txt_name);
            description = (TextView) v.findViewById(R.id.txt_description);
            forks = (TextView) v.findViewById(R.id.txt_forks);
            stars = (TextView) v.findViewById(R.id.txt_stars);
            avatar = (ImageView) v.findViewById(R.id.img_avatar);
            owner = (TextView) v.findViewById(R.id.txt_owner);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        FooterViewHolder(final View itemView) {
            super(itemView);
        }
    }
}