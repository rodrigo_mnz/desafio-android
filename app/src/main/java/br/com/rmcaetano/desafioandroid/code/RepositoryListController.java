package br.com.rmcaetano.desafioandroid.code;

import android.content.Intent;

import org.parceler.Parcels;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import br.com.rmcaetano.desafioandroid.network.Api;
import br.com.rmcaetano.desafioandroid.network.Repository;
import br.com.rmcaetano.desafioandroid.network.RepositoryList;
import br.com.rmcaetano.desafioandroid.ui.PullRequestsActivity;
import br.com.rmcaetano.desafioandroid.ui.RepositoryListActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepositoryListController extends BaseController implements LoadDataCallbacks, ListItemClickListener<Repository> {

    private WeakReference<RepositoryListActivity> activity = new WeakReference<>(null);
    private final RepositoryListAdapter adapter;
    private int lastPageLoaded;

    public RepositoryListController(RepositoryListActivity activity, RepositoryListAdapter adapter) {
        this.activity = new WeakReference<>(activity);
        this.adapter = adapter;

        this.adapter.setLoadDataCallbacks(this);
        this.adapter.setOnItemClickListener(this);
    }

    @Override
    public void onLoadMore() {
        lastPageLoaded++;
        fetchRepositories(lastPageLoaded);
    }

    @Override
    public void onRetry() {
        fetchRepositories(lastPageLoaded);
    }

    @SuppressWarnings("unused")
    @Override
    public void onItemClick(Repository item) {
        Intent intent = new Intent(activity.get(), PullRequestsActivity.class);
        intent.putExtra(PullRequestsActivity.REPOSITORY, Parcels.wrap(item));
        activity.get().startActivity(intent);
    }

    public void fetchRepositories(final int page) {
        lastPageLoaded = page;

        if (page == 1) {
            showProgressBar(activity.get(), true);
        }

        Api.getService().getRepositories(page).enqueue(new Callback<RepositoryList>() {
            @Override
            public void onResponse(Call<RepositoryList> call, Response<RepositoryList> response) {
                final RepositoryList repositoryList = response.body();

                if (activity.get() != null && repositoryList != null && !activity.get().isFinishing()) {
                    updateAdapter(repositoryList.items, page);
                }

                if (page == 1) {
                    showProgressBar(activity.get(), false);
                }
            }

            @Override
            public void onFailure(Call<RepositoryList> call, Throwable t) {
                t.printStackTrace();

                showRetryDialog(activity.get(), RepositoryListController.this);

                if (page == 1) {
                    showProgressBar(activity.get(), false);
                }
            }
        });
    }

    private void updateAdapter(ArrayList<Repository> repositoryList, int page) {
        if (page == 1) {
            adapter.setData(repositoryList);
        } else {
            adapter.addToData(repositoryList);
        }
    }
}