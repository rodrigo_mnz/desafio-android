package br.com.rmcaetano.desafioandroid.network;

import com.google.gson.annotations.SerializedName;

@org.parceler.Parcel
public class PullRequest {

    public int id;
    public int number;
    public String state;
    public String title;
    public String body;
    public Owner user;

    @SerializedName("html_url")
    public String htmlUrl;

    @SerializedName("created_at")
    public String createdAt;
}