package br.com.rmcaetano.desafioandroid.network;

import com.google.gson.annotations.SerializedName;

@org.parceler.Parcel
public class Repository {

    public int id;
    public String name;
    public Owner owner;
    public String description;

    @SerializedName("stargazers_count")
    public int stargazersCount;

    @SerializedName("forks_count")
    public int forksCount;

    @SerializedName("full_name")
    public String fullName;
}