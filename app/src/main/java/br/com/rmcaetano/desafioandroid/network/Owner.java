package br.com.rmcaetano.desafioandroid.network;

import com.google.gson.annotations.SerializedName;

@org.parceler.Parcel
public class Owner {

    public int id;
    public String login;

    @SerializedName("avatar_url")
    public String avatarUrl;
}