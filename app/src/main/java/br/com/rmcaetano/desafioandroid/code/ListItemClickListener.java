package br.com.rmcaetano.desafioandroid.code;

public interface ListItemClickListener<T> {

    void onItemClick(T item);
}