package br.com.rmcaetano.desafioandroid.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.parceler.Parcels;

import java.util.ArrayList;

import br.com.rmcaetano.desafioandroid.R;
import br.com.rmcaetano.desafioandroid.code.PullRequestsAdapter;
import br.com.rmcaetano.desafioandroid.code.PullRequestsController;
import br.com.rmcaetano.desafioandroid.network.PullRequest;
import br.com.rmcaetano.desafioandroid.network.Repository;

public class PullRequestsActivity extends BaseActivity {

    public static final String REPOSITORY = "REPOSITORY";
    public static final String DATA = "DATA";
    private PullRequestsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pullrequests);

        Repository repository = Parcels.unwrap(getIntent().getExtras().getParcelable(REPOSITORY));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new PullRequestsAdapter(this);
        recyclerView.setAdapter(adapter);

        PullRequestsController controller = new PullRequestsController(this, adapter);

        if (repository != null) {
            setTitle(repository.fullName);
        }

        if (savedInstanceState != null) {
            ArrayList<PullRequest> data = Parcels.unwrap(savedInstanceState.getParcelable(DATA));
            adapter.setData(data);
        } else if (repository != null) {
            controller.fetchPullRequests(repository.owner.login, repository.name, 1);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(DATA, Parcels.wrap(adapter.getData()));
        super.onSaveInstanceState(outState);
    }
}