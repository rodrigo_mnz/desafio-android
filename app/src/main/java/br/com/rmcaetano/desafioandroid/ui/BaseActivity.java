package br.com.rmcaetano.desafioandroid.ui;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import br.com.rmcaetano.desafioandroid.R;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    protected ProgressBar progressBar;

    public void showProgressBar(boolean show) {
        if (progressBar == null) {
            progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        }

        if (progressBar != null) {
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
}