package br.com.rmcaetano.desafioandroid.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;

import br.com.rmcaetano.desafioandroid.BuildConfig;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

public class Api {

    private static final String BASE_URL = "https://api.github.com";
    public static final int MAX_SIZE = 10 * 1024 * 1024;
    public static final int CACHE_MAX_AGE = 60;
    public static final int CACHE_MAX_AGE_WITHOUT_NETWORK = 60 * 60 * 24 * 7;
    private static ApiService mApiService;
    private static MockInterceptor mockInterceptor;

    public static void initApi(final Context context) {
        Interceptor cache = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                if (isNetworkAvailable(context)) {
                    request = request.newBuilder().header("Cache-Control", "public, max-age=" + CACHE_MAX_AGE).build();
                } else {
                    request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + CACHE_MAX_AGE_WITHOUT_NETWORK).build();
                }
                return chain.proceed(request);
            }
        };

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(BuildConfig.DEBUG ? BODY : NONE);

        OkHttpClient client = new OkHttpClient
                .Builder()
                .cache(new Cache(context.getCacheDir(), MAX_SIZE))
                .addInterceptor(new GzipRequestInterceptor())
                .addInterceptor(logging)
                .addInterceptor(cache)
                .build();

        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mApiService = mRetrofit.create(ApiService.class);
    }

    public static ApiService getService() {
        return mApiService;
    }

    private static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public static void initMock(Context context) {
        if (mockInterceptor == null) {
            mockInterceptor = new MockInterceptor(context);
        }

        OkHttpClient client = new OkHttpClient
                .Builder()
                .cache(new Cache(context.getCacheDir(), MAX_SIZE))
                .addInterceptor(mockInterceptor)
                .build();

        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mApiService = mRetrofit.create(ApiService.class);
    }

    public static void setMockError(boolean error) {
        if (mockInterceptor != null) {
            mockInterceptor.forceError(error);
        }
    }
}