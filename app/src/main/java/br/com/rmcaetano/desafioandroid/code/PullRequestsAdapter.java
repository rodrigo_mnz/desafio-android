package br.com.rmcaetano.desafioandroid.code;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.rmcaetano.desafioandroid.R;
import br.com.rmcaetano.desafioandroid.network.PullRequest;

public class PullRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String CLIENT_DATE_FORMAT = "dd/MM/yy HH:mm";
    private static final int NORMAL_VIEW = 0;
    private static final int FOOTER_VIEW = 1;
    public static final int ITEMS_PER_PAGE = 30;
    private ArrayList<PullRequest> data;
    private final int colorAccent;
    private final int colorGrey;
    private final int colorWhite;
    private final int colorClosed;
    private final int colorOpen;
    private final SimpleDateFormat serverDateFormat;
    private final SimpleDateFormat clientDateFormat;
    private ListItemClickListener<PullRequest> listener;
    private boolean probablyHasMoreItems;
    private LoadDataCallbacks loadDataCallbacks;

    public PullRequestsAdapter(Context context) {
        colorAccent = ResourcesCompat.getColor(context.getResources(), R.color.text_accent, context.getTheme());
        colorGrey = ResourcesCompat.getColor(context.getResources(), R.color.grey, context.getTheme());
        colorWhite = ResourcesCompat.getColor(context.getResources(), R.color.white, context.getTheme());
        colorClosed = ResourcesCompat.getColor(context.getResources(), R.color.red, context.getTheme());
        colorOpen = ResourcesCompat.getColor(context.getResources(), R.color.green, context.getTheme());
        serverDateFormat = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.ENGLISH);
        clientDateFormat = new SimpleDateFormat(CLIENT_DATE_FORMAT, Locale.ENGLISH);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == NORMAL_VIEW) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_pullrequest, parent, false);
            return new ItemViewHolder(v);
        } else if (viewType == FOOTER_VIEW) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_progressbar, parent, false);
            return new FooterViewHolder(v);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (getItemViewType(position) == NORMAL_VIEW) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            final PullRequest pullRequest = data.get(position);
            final Context context = holder.itemView.getContext();

            itemViewHolder.name.setText(generateTitle(pullRequest));

            itemViewHolder.description.setText(pullRequest.body);
            itemViewHolder.owner.setText(pullRequest.user.login);

            final String date = generateDate(pullRequest.createdAt);
            itemViewHolder.date.setText(date);

            Picasso.with(context)
                    .load(pullRequest.user.avatarUrl)
                    .placeholder(R.drawable.ic_person)
                    .into(itemViewHolder.avatar);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClick(pullRequest);
                    }
                }
            });
        } else if (getItemViewType(position) == FOOTER_VIEW) {

            if (loadDataCallbacks != null) {
                loadDataCallbacks.onLoadMore();
            }
        }
    }

    @Override
    public int getItemViewType(final int position) {

        if ((position == data.size()))
            return FOOTER_VIEW;

        return NORMAL_VIEW;
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }

        return probablyHasMoreItems ? data.size() + 1 : data.size();
    }

    public void setData(ArrayList<PullRequest> data) {
        if (data != null) {
            probablyHasMoreItems = data.size() >= ITEMS_PER_PAGE;

            sortOpenPullRequestFirst(data);

            this.data = data;
            notifyDataSetChanged();
        }
    }

    public void sortOpenPullRequestFirst(ArrayList<PullRequest> data) {
        Collections.sort(data, new Comparator<PullRequest>() {
            @Override
            public int compare(PullRequest o1, PullRequest o2) {
                return o2.state.compareTo(o1.state);
            }
        });
    }

    public ArrayList<PullRequest> getData() {
        return data;
    }

    public void addToData(List<PullRequest> data) {
        if (data != null) {
            probablyHasMoreItems = data.size() >= ITEMS_PER_PAGE;

            final int initialSize = this.data.size();
            this.data.addAll(data);
            notifyItemRangeChanged(initialSize, this.data.size());
        }
    }

    public void setLoadDataCallbacks(LoadDataCallbacks loadDataCallbacks) {
        this.loadDataCallbacks = loadDataCallbacks;
    }

    public void setOnItemClickListener(ListItemClickListener<PullRequest> listener) {
        this.listener = listener;
    }

    public String generateDate(String createdAt) {
        try {
            final Date date = serverDateFormat.parse(createdAt);
            return clientDateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Spannable generateTitle(PullRequest pullRequest) {
        final String stateText = " " + pullRequest.state + "  ";
        final String numberText = " #" + pullRequest.number;
        final String fullText = stateText + pullRequest.title + numberText;

        Spannable name = new SpannableString(fullText);
        Log.i(getClass().getSimpleName(), "fullText: " + fullText);

        if (pullRequest.state.equalsIgnoreCase("OPEN")) {
            name.setSpan(new BackgroundColorSpan(colorOpen), 0, stateText.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            name.setSpan(new ForegroundColorSpan(colorWhite), 0, stateText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else if (pullRequest.state.equalsIgnoreCase("CLOSED")) {
            name.setSpan(new BackgroundColorSpan(colorClosed), 0, stateText.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            name.setSpan(new ForegroundColorSpan(colorWhite), 0, stateText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        name.setSpan(new ForegroundColorSpan(colorAccent), stateText.length(), stateText.length() + pullRequest.title.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        name.setSpan(new ForegroundColorSpan(colorGrey), stateText.length() + pullRequest.title.length(), stateText.length() + pullRequest.title.length() + numberText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return name;
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        final TextView name;
        final TextView description;
        final TextView owner;
        final TextView date;
        final ImageView avatar;

        ItemViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.txt_name);
            description = (TextView) v.findViewById(R.id.txt_description);
            avatar = (ImageView) v.findViewById(R.id.img_avatar);
            owner = (TextView) v.findViewById(R.id.txt_owner);
            date = (TextView) v.findViewById(R.id.txt_date);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        FooterViewHolder(final View itemView) {
            super(itemView);
        }
    }
}